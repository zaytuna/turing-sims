package turing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import math.GenVector;
import math.Matrix;
import utils.Methods;

public class RingSimulation extends JPanel implements Runnable, ActionListener {
	private static final long serialVersionUID = -7371790493929873710L;

	private int NPTS, NVARS;
	Color[] colors;

	private double[][] u; // morphogens
	private Matrix jacob;
	private double[] D;

	private double DX, DT = 0.05;
	boolean paused = true;
	boolean logistic = false;

	// JCOmponents
	JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER));
	JTextField field = new JTextField();
	JButton change = new JButton("Change");
	JButton reset = new JButton("Reset");

	public RingSimulation(double[][] lin, double[] d) {
		setup(lin, d);

		// setup components
		controls.add(field);
		controls.add(change);
		controls.add(reset);

		field.setPreferredSize(new Dimension(200, 25));
		change.setPreferredSize(new Dimension(100, 25));
		reset.setPreferredSize(new Dimension(100, 25));
		field.setHorizontalAlignment(JTextField.CENTER);

		field.addActionListener(this);
		change.addActionListener(this);
		reset.addActionListener(this);
	}

	private void setup(double[][] lin, double[] d) {
		if (lin == null || d == null)
			return;

		NPTS = 800;
		NVARS = d.length;

		this.u = new double[NPTS][NVARS];
		this.colors = new Color[NVARS];
		this.D = d;
		this.DX = 2 * Math.PI / NPTS;

		for (int j = 0; j < NVARS; j++) {
			colors[j] = Methods.randomColor().darker();

			for (int i = 0; i < NPTS; i++) {
				u[i][j] = (Math.random() - 0.5) / 10;
			}
		}

		jacob = new Matrix(lin);

		invdiff = new Matrix[NVARS];
		scales = new double[NVARS];

		computeInverses(DT);

		paused = false;
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == field || evt.getSource() == change) {
			try {
				new Thread(new Runnable() {
					public void run() {
						reload(field.getText());
					}
				}).start();
			} catch (Exception e) {}
		} else if (evt.getSource() == reset) {
			paused = true;
			try {
				Thread.sleep(10);
			} catch (Exception e) {}
			for (int j = 0; j < NVARS; j++) {
				colors[j] = Methods.randomColor();

				for (int i = 0; i < NPTS; i++) {
					u[i][j] = (Math.random() - 0.5) / 10;
				}
			}
			paused = false;

		}
	}

	public void reload(String filename) {
		paused = true;
		String[] lines =
				Methods.getFileContents(new File(filename)).split("\n");

		String[] diff = lines[0].split("\t");
		double[] d = new double[diff.length];
		for (int i = 0; i < d.length; i++)
			d[i] = Double.parseDouble(diff[i]);

		double[][] jac = new double[d.length][d.length];

		for (int q = 0; q < d.length; q++) {
			String[] pieces = lines[q + 1].split("\t");

			for (int r = 0; r < d.length; r++)
				jac[q][r] = Double.parseDouble(pieces[r]);
		}
		try {
			Thread.sleep(1);
		} catch (Exception e) {}

		setup(jac, d);
		paused = false;
	}

	public static RingSimulation load(String filename) {
		RingSimulation rs =
				new RingSimulation(new double[][] {}, new double[] {});
		rs.reload(filename);
		rs.field.setText(filename);
		return rs;
	}

	private Matrix[] invdiff;
	private double[] scales;

	private void computeInverses(double dt) {
		for (int j = 0; j < NVARS; j++) {
			invdiff[j] = Matrix.createID(NPTS);
			scales[j] = D[j] * dt / (2 * DX * DX);
			System.out.println(scales[j]);

			for (int i = 0; i < NPTS; i++) {
				invdiff[j].set(i, i, 1 + scales[j]);

				invdiff[j].set(i, (i + 1) % NPTS, -scales[j] / 2);
				invdiff[j].set((i + 1) % NPTS, i, -scales[j] / 2);
			}

			invdiff[j] = invdiff[j].calculateInverse();
		}

	}

	public void update(double dt) {
		double[][] newu = new double[NPTS][NVARS];

		for (int j = 0; j < NVARS; j++) {
			if (paused)
				return;

			double[] app = new double[NPTS];
			for (int i = 0; i < NPTS; i++) {
				// forward Euler piece of CN
				double fe = scales[j] * (u[(i + 1) % NPTS][j] -
						2 * u[i][j] + u[(i + NPTS - 1) % NPTS][j]);
				app[i] = u[i][j] + fe / 2;
			}

			GenVector v = invdiff[j].applyTo(new GenVector(app));

			for (int i = 0; i < NPTS; i++) {
				newu[i][j] = v.get(i);
			}
		}

		for (int i = 0; i < NPTS; i++) {
			if (paused)
				return;
			GenVector v = jacob.applyTo(new GenVector(newu[i]));

			for (int j = 0; j < NVARS; j++) {
				double factor = 1;
				if (logistic)
					factor = (1 - u[i][j]) * (-1 - u[i][j]);

				newu[i][j] += dt * (v.get(j)) * factor;

				if (Math.abs(newu[i][j]) > 0.8)
					newu[i][j] *= Math.exp((0.8 - Math.abs(newu[i][j])) / 100);
			}
		}

		for (int i = 0; i < NPTS; i++)
			for (int j = 0; j < NVARS; j++)
				u[i][j] = newu[i][j];

	}
	public void run() {
		while (true) {
			try {
				Thread.sleep(1);
			} catch (Exception e) {}

			if (!paused)
				update(DT);

			repaint();

		}
	}

	@Override
	public void paintComponent(Graphics gg) {
		super.paintComponent(gg);

		Graphics2D g = (Graphics2D) gg;

		if (paused) {

		} else {
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			int cx = getWidth() / 2, cy = getHeight() / 2, r =
					Math.min(getWidth(), getHeight()) / 5;

			g.setColor(Color.black);

			g.drawOval(cx - r, cy - r, 2 * r, 2 * r);

			int[] px = new int[NVARS], py = new int[NVARS]; // history variables, to draw lines
			for (int i = 0; i <= NPTS; i++) {
				double theta = 2 * Math.PI * i / NPTS;

				for (int j = 0; j < NVARS; j++) {
					double rad =
							r * (1 + Math.atan(u[i % NPTS][j]) / (Math.PI / 2));
					int dux = (int) (Math.cos(theta) * rad);
					int duy = (int) (Math.sin(theta) * rad);

					g.setColor(colors[j]);
					// g.fillOval(cx + dux - 5, cy + duy - 5, 10, 10);

					if (i != 0)
						g.drawLine(cx + px[j], cy + py[j], cx + dux, cy + duy);

					px[j] = dux;
					py[j] = duy;
				}
			}
		}
	}
	public static void main(String[] args) {
		RingSimulation rs = RingSimulation.load("Resources\\A.sim");

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setUndecorated(true);
		frame.add(rs, java.awt.BorderLayout.CENTER);
		frame.add(rs.controls, java.awt.BorderLayout.NORTH);
		frame.setVisible(true);

		new Thread(rs).start();
	}
}
