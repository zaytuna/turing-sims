package turing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

import math.GenVector;
import math.Matrix;
import utils.Methods;

public class Sim2D extends JPanel implements Runnable, ActionListener {
	private static final long serialVersionUID = -7371790493929873710L;
	public static final Color[] COLORS = { new Color(250, 250, 0),
			Color.BLACK,
			Color.GREEN };
	public static final Color[] COLORS2 = { new Color(250, 0, 250),
		Color.ORANGE,
		Color.YELLOW };	
	
	public static final double CAP = 1;

	private int W, H, NVARS;
	Color[] colors;

	private double[][][] u; // morphogens
	private Matrix jacob;
	private double[] D;

	private double DX, DT = 0.05;

	// JCOmponents
	JPanel controls = new JPanel(new FlowLayout(FlowLayout.CENTER));
	JTextField field = new JTextField();
	JButton change = new JButton("Change");
	JButton reset = new JButton("Reset");
	JButton rBump = new JButton("Bump");
	JButton rOffset = new JButton("Biased");
	JCheckBox box = new JCheckBox("Circles");

	boolean newData = false;
	boolean paused = true;

	public Sim2D(double[][] lin, double[] d) {
		setup(lin, d);

		// setup components
		controls.add(field);
		controls.add(change);
		controls.add(reset);
		controls.add(rBump);
		controls.add(rOffset);
		controls.add(box);

		field.setPreferredSize(new Dimension(200, 25));
		change.setPreferredSize(new Dimension(100, 25));
		reset.setPreferredSize(new Dimension(100, 25));
		field.setHorizontalAlignment(JTextField.CENTER);
		rBump.setPreferredSize(new Dimension(100, 25));
		rOffset.setPreferredSize(new Dimension(100, 25));

		field.addActionListener(this);
		change.addActionListener(this);
		reset.addActionListener(this);
		rBump.addActionListener(this);
		rOffset.addActionListener(this);
	}

	private void setup(double[][] lin, double[] d) {
		if (lin == null || d == null)
			return;

		W = 350;
		H = 250;
		NVARS = d.length;

		this.u = new double[W][H][NVARS];
		this.colors = new Color[NVARS];
		this.D = d;
		this.DX = 1.0d / H;

		for (int j = 0; j < NVARS; j++) {
			// colors[j] = Methods.randomColor().darker();
			colors[j] = COLORS[j % NVARS];

			for (int i = 0; i < W; i++) {
				for (int k = 0; k < H; k++)
					u[i][k][j] = (Math.random() - 0.5) * 2;
			}
		}

		jacob = new Matrix(lin);

		invdiffX = new Matrix[NVARS];
		invdiffY = new Matrix[NVARS];
		scales = new double[NVARS];

		computeInverses(DT);

		paused = false;
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource() == field || evt.getSource() == change) {
			paused = true;
			try {
				Thread.sleep(100);
				new Thread(new Runnable() {
					public void run() {
						reload(field.getText());
						paused = false;
					}
				}).start();
			} catch (Exception e) {}
		} else if (evt.getSource() == reset) {
			paused = true;
			try {
				Thread.sleep(100);
			} catch (Exception e) {}

			for (int j = 0; j < NVARS; j++) {
				for (int i = 0; i < W; i++) {
					newData = true;
					for (int k = 0; k < H; k++)
						u[i][k][j] = (Math.random() - 0.5) / 10;
				}
			}

			paused = false;
		} else if (evt.getSource() == rBump) {
			paused = true;
			try {
				Thread.sleep(100);
			} catch (Exception e) {}

			for (int j = 0; j < NVARS; j++) {
				for (int i = 0; i < W; i++) {
					newData = true;

					for (int k = 0; k < H; k++)
						u[i][k][j] =
								Math.exp(-((k - H / 2) * (k - H / 2) + (i - W / 2)
										* (i - W / 2)) / 100.0);
				}
			}
			paused = false;
		} else if (evt.getSource() == rOffset) {
			paused = true;
			try {
				Thread.sleep(100);
			} catch (Exception e) {}

			for (int j = 0; j < NVARS; j++) {
				for (int i = 0; i < W; i++) {
					newData = true;
					for (int k = 0; k < H; k++)
						u[i][k][j] = (Math.random()) / 10;
				}
			}

			paused = false;
		}
	}
	public void reload(String filename) {
		newData = true;
		String[] lines =
				Methods.getFileContents(new File(filename)).split("\n");

		String[] diff = lines[0].split("\t");
		double[] d = new double[diff.length];
		for (int i = 0; i < d.length; i++)
			d[i] = Double.parseDouble(diff[i]);

		double[][] jac = new double[d.length][d.length];

		for (int q = 0; q < d.length; q++) {
			String[] pieces = lines[q + 1].split("\t");

			for (int r = 0; r < d.length; r++)
				jac[q][r] = Double.parseDouble(pieces[r]);
		}
		try {
			Thread.sleep(1);
		} catch (Exception e) {}

		setup(jac, d);
	}

	public static Sim2D load(String filename) {
		Sim2D rs =
				new Sim2D(new double[][] {}, new double[] {});
		rs.reload(filename);
		rs.field.setText(filename);
		return rs;
	}

	private Matrix[] invdiffX;
	private Matrix[] invdiffY;
	private double[] scales;

	private void computeInverses(double dt) {
		for (int j = 0; j < NVARS; j++) {
			invdiffX[j] = Matrix.createID(W);
			invdiffY[j] = Matrix.createID(H);
			scales[j] = D[j] * dt / (2 * DX * DX);
			System.out.println(scales[j]);

			for (int i = 0; i < W; i++) {
				invdiffX[j].set(i, i, 1 + scales[j]);
				invdiffX[j].set(i, (i + 1) % W, -scales[j] / 2);
				invdiffX[j].set((i + 1) % W, i, -scales[j] / 2);
			}

			for (int i = 0; i < H; i++) {
				invdiffY[j].set(i, i, 1 + scales[j]);
				invdiffY[j].set(i, (i + 1) % H, -scales[j] / 2);
				invdiffY[j].set((i + 1) % H, i, -scales[j] / 2);
			}

			invdiffX[j] = invdiffX[j].calculateInverse();
			invdiffY[j] = invdiffY[j].calculateInverse();
		}
	}

	public void update(double dt) {
		newData = false;
		double[][][] newu = new double[W][H][NVARS];
		System.out.println(u[0][0][0]);

		for (int j = 0; j < NVARS; j++) {
			// DIFFUSE X
			for (int k = 0; k < H; k++) {
				double[] app = new double[W];
				for (int i = 0; i < W; i++) {
					// forward Euler piece of CN
					double fe = scales[j] * (u[(i + 1) % W][k][j] -
							2 * u[i][k][j] + u[(i + W - 1) % W][k][j]);
					app[i] = u[i][k][j] + fe / 2;
				}

				GenVector v = invdiffX[j].applyTo(new GenVector(app));

				for (int i = 0; i < W; i++) {
					newu[i][k][j] += v.get(i);
				}
			}

			// DIFFUSE Y
			for (int i = 0; i < W; i++) {
				double[] app = new double[H];
				for (int k = 0; k < H; k++) {
					// forward Euler piece of CN
					double fe = scales[j] * (u[i][(k + 1) % H][j] -
							2 * u[i][k][j] + u[i][(k + H - 1) % H][j]);
					app[k] = u[i][k][j] + fe / 2;
				}

				GenVector v = invdiffY[j].applyTo(new GenVector(app));

				for (int k = 0; k < H; k++) {
					newu[i][k][j] += v.get(k);
					newu[i][k][j] /= 2;
				}
			}
		}

		for (int k = 0; k < H; k++) {
			for (int i = 0; i < W; i++) {
				GenVector v = jacob.applyTo(new GenVector(newu[i][k]));

				for (int j = 0; j < NVARS; j++) {
					newu[i][k][j] += dt * (v.get(j));

					if (Math.abs(newu[i][k][j]) > CAP)
						newu[i][k][j] *=
								Math.exp((CAP - Math.abs(newu[i][k][j])) / 100);
				}
			}
		}

		if (!newData)
			u = newu;
	}

	public void run() {
		while (true) {
			try {
				Thread.sleep(1);
			} catch (Exception e) {}

			if (!paused)
				update(DT);

			repaint();

		}
	}

	@Override
	public void paintComponent(Graphics gg) {
		super.paintComponent(gg);

		Graphics2D g = (Graphics2D) gg;

		if (!paused)
		{
			g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);

			int dw = Math.min(getWidth() / W, getHeight() / H);
			int ofx = (getWidth() - dw * W) / 2, ofy =
					(getHeight() - dw * H) / 2;

			for (int i = 0; i < W; i++)
				for (int k = 0; k < H; k++) {
					int j = 0;

					if (box.isSelected()) {
						// for (; j < NVARS; j++) {

						int r = (int) (dw / (3 - Math.min(u[i][k][j], 2)));

						g.setColor(colors[j]);
						g.fillOval(ofx + i * dw + dw / 2 - r / 2, ofy + k
								* dw + dw / 2 - r / 2 - r, r, r);
						// }

					} else {
						double value =
								Math.atan(u[i][k][j]) / (Math.PI / 2);
						if (value >= 0)
							g.setColor(Methods.colorMeld(Color.WHITE,
									colors[j],
									value));
						else
							g.setColor(Methods.colorMeld(Color.WHITE,
									COLORS2[j%NVARS],
									-value));

						// int s = (int)Math.sqrt(NVARS)+1;
						// g.fillRect(ofx + i * dw+((j%s)*dw/s), ofy + k * dw+((j/s)*dw/s),
						// dw/s, dw/s);
						g.fillRect(ofx + i * dw, ofy + k * dw, dw, dw);
					}
				}
		}
	}
	public static void main(String[] args) {
		Sim2D rs = Sim2D.load("Resources\\Test.sim");

		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setUndecorated(true);
		frame.add(rs, java.awt.BorderLayout.CENTER);
		frame.add(rs.controls, java.awt.BorderLayout.NORTH);
		frame.setVisible(true);

		new Thread(rs).start();
	}
}
